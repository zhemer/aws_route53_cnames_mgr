# aws_route53_cnames_mgr

aws_route53_cnames_mgr allows to create/delete CNAME records from the AWS Route53. The primary intent was to allow fast restore records deleted by accident from backuped JSON file (specified by -file parameter). The utility also can be used to create/delete Route53 CNAME records selecting its set by command line parameters or specifying a set of records in a JSON file. The parameter '-r' runs commands, without it a preview mode will be used that outputs actions on console.
In order for the aws_route53_cnames_mgr to operate properly ~/.aws/config and ~/.aws/credentials must be properly set up. It can be tested with aws_route53_cnames by listing zone records using -l and -z parameters.
```shell
user @ user-nb : ~/.aws 
$ tail *
==> config <==
[default]
output = table
region = us-east-1

==> credentials <==
[default]
aws_access_key_id = iddqd_access
aws_secret_access_key = iddqd_secret
```

JSON file example
```shell
{
  "ZoneId": "Z0123456789",
  "Org": "idSoftware",
  "Services": ["Doom", "Quake", "Rage"]
}
```

Help screen:
```shell
$ ./aws_route53_cnames_mgr.go 
You must specify one of mandatory switch: '-c', '-d', '-l' and required options.
aws_route53_cnames_mgr Creates/deletes AWS Route53 records CNAME records.
Whenever '-file' parameter is specified for JSON parameters, that parameters ("ZoneId" = '-z', "Org" = '-org', "Services" = '-svc') will be used for create/delete ('-c'/'-d') mode instead appropriate command line parameters.
For the list ('-l') mode only "ZoneId" is needed.
Switch '-r' runs commands for create/delete mode.
Version 0.0.1
Usage:
  Create records:
	aws_route53_cnames_mgr -c -z <ZoneId> -org <name> -svc <service1[,service2,...]>
	Using ZoneId, Org, Services from JSON file: aws_route53_cnames_mgr -c -file <JSON file>
  Delete records:
	aws_route53_cnames_mgr -d -z <ZoneId> -org <name> -svc <service1[,service2,...]> -file <JSON file>
  List records:
	aws_route53_cnames_mgr -l -z <ZoneId>
	Using ZoneId from JSON file: aws_route53_cnames_mgr -l -file <JSON file>

JSON file format example:
{
  "ZoneId": "Z0123456789",
  "ZoneDescr": "Gaming zone",
  "Org": "idSoftware",
  "Services": ["Doom", "Quake", "Rage"]
}

Parameters:
  -c	Create records
  -d	Delete records
  -dest string
    	Where to route traffic (default "nlb.service.consul")
  -file string
    	JSON file for create/delete records (.json extension will be added)
  -l	List zone
  -org string
    	Organization name
  -r	Run commands
  -svc string
    	Organization services list (comma separated)
  -ttl int
    	TTL for DNS Cache (default 300)
  -v	Tune on verbose output
  -z string
    	AWS Zone Id for domain

```

Usage example
```shell
$ go run aws_route53_cnames_mgr.go -c -z Z0123456789 -org idSoftware -svc doom,quake,rage
Using zone: Hosted zone ID: "Z0123456789", Domain name: "service.consul.", Description: "Gaming zone"
Record "doom-idSoftware.service.consul." will be UPSERTed
Record "quake-idSoftware.service.consul." will be UPSERTed
Record "rage-idSoftware.service.consul." will be UPSERTed

$ go run aws_route53_cnames_mgr.go -c -z Z0123456789 -org idSoftware -svc doom,quake,rage -r
Using zone: Hosted zone ID: "Z0123456789", Domain name: "service.consul.", Description: "Gaming zone"
Record "doom-idSoftware.service.consul." was UPSERTed
Record "quake-idSoftware.service.consul." was UPSERTed
Record "rage-idSoftware.service.consul." was UPSERTed

$ go run aws_route53_cnames_mgr.go -l -z Z0123456789
Using zone: Hosted zone ID: "Z0123456789", Domain name: "service.consul.", Description: "Gaming zone"
Skipping NS service.consul.
Skipping SOA service.consul.
Record name "doom-idsoftware.service.consul." routeTo "nlb.service.consul" ttl 300 type CNAME
Record name "quake-idsoftware.service.consul." routeTo "nlb.service.consul" ttl 300 type CNAME
Record name "rage-idsoftware.service.consul." routeTo "nlb.service.consul" ttl 300 type CNAME

$ go run aws_route53_cnames_mgr.go -d -z Z0123456789 -org idsoftware -svc doom,quake,rage -file backup -r
Using zone: Hosted zone ID: "Z0123456789", Domain name: "service.consul.", Description: "Gaming zone"
Record "doom-idsoftware.service.consul." was DELETEed
Record "quake-idsoftware.service.consul." was DELETEed
Record "rage-idsoftware.service.consul." was DELETEed

$ jq <backup.json 
{
  "ZoneId": "Z0123456789",
  "ZoneDescr": "Gaming zone",
  "Org": "idsoftware",
  "Services": [
    "doom",
    "quake",
    "rage"
  ]
}

```
