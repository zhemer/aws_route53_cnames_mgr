package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/route53"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

const (
	sVer   = "0.0.1"
	sCname = "CNAME"
	sDomC  = "service.consul"
	sDestC = "nlb.service.consul"
	sJson  = ".json"
)

var (
	sDom    *string
	sDest   = flag.String("dest", sDestC, "Where to route traffic")
	sZoneId = flag.String("z", "", "AWS Zone Id for domain")
	TTL     = flag.Int64("ttl", int64(300), "TTL for DNS Cache")
	sOrg    = flag.String("org", "", "Organization name")
	sSvc    = flag.String("svc", "", "Organization services list (comma separated)")
	iCr     = flag.Bool("c", false, "Create records")
	iDel    = flag.Bool("d", false, "Delete records")
	iList   = flag.Bool("l", false, "List zone")
	sFile   = flag.String("file", "", "JSON file for create/delete records (.json extension will be added)")
	iDebug  = flag.Bool("v", false, "Tune on verbose output")
	// iForce = flag.Bool("F", false, "Force operations despite errors")
	iRun = flag.Bool("r", false, "Run commands")
)

type AwsZone struct {
	ZoneId    string
	ZoneDescr string
	Org       string
	Services  []string
}

var az AwsZone

func main() {
	flag.Usage = func() {
		self := strings.Split(os.Args[0], "/")
		sMe := self[len(self)-1]
		fmt.Fprintf(flag.CommandLine.Output(), `%v Creates/deletes AWS Route53 records %v records.
Whenever '-file' parameter is specified for JSON parameters, that parameters ("ZoneId" = '-z', "Org" = '-org', "Services" = '-svc') will be used for create/delete ('-c'/'-d') mode instead appropriate command line parameters.
For the list ('-l') mode only "ZoneId" is needed.
Switch '-r' runs commands for create/delete mode.
Version %s
Usage:
  Create records:
	%s -c -z <ZoneId> -org <name> -svc <service1[,service2,...]>
	Using ZoneId, Org, Services from JSON file: %s -c -file <JSON file>
  Delete records:
	%s -d -z <ZoneId> -org <name> -svc <service1[,service2,...]> -file <JSON file>
  List records:
	%s -l -z <ZoneId>
	Using ZoneId from JSON file: %s -l -file <JSON file>

JSON file format example:
{
  "ZoneId": "Z0123456789",
  "ZoneDescr": "Gaming zone",
  "Org": "idSoftware",
  "Services": ["Doom", "Quake", "Rage"]
}

Parameters:
`, sMe, sCname, sVer, sMe, sMe, sMe, sMe, sMe)
		flag.PrintDefaults()
	}

	flag.Parse()
	sum := b2i(*iCr) + b2i(*iDel) + b2i(*iList)
	sum1 := s2i(*sZoneId) + s2i(*sOrg) + s2i(*sSvc)
	Log("sum %v sum1 %v\n", sum, sum1)
	switch {
	case *iList && *sZoneId != "" && sum1 == 1:
		break
	case sum != 1:
		fallthrough
	case *iDel && *sFile == "":
		fallthrough
	case sum1 != 3 && *sFile == "":
		fmt.Fprintf(flag.CommandLine.Output(), "You must specify one of mandatory switch: '-c', '-d', '-l' and required options.\n")
		flag.Usage()
		os.Exit(1)
	}

	if *sFile != "" && !strings.HasSuffix(*sFile, sJson) {
		*sFile = *sFile + sJson
	}
	Log("sFile %q\n", *sFile)

	// Reading AWS Route53 zone data from the sFile
	if (*iCr || *iList) && *sFile != "" {
		data, err := ioutil.ReadFile(*sFile)
		if err != nil {
			log.Fatalf("ReadFile: %v: %q\n", err, *sFile)
		}
		if !json.Valid(data) {
			log.Fatal(*sFile + ": invalid JSON format\n")
		}
		err = json.Unmarshal([]byte(data), &az)
		if err != nil {
			log.Fatal(err)
		}
		Log("az %+v\n", az)
		if az.ZoneId == "" {
			log.Fatal("ZoneId can't be empty")
		}
	} else {
		az.ZoneId = *sZoneId
		az.Org = *sOrg
		az.Services = strings.Split(*sSvc, ",")
	}

	// Creating AWS session and getting Route53 session
	sess, err := session.NewSession()
	if err != nil {
		log.Fatalf("Failed to create a session: %v, exiting\n", err)
	}
	svc := route53.New(sess)
	// Get zone domain by id
	input := &route53.GetHostedZoneInput{
		Id: aws.String(az.ZoneId),
	}
	result, err := svc.GetHostedZone(input)
	if err != nil {
		log.Fatalf("Error: %v, exiting\n", err)
	}
	Log("GetHostedZone %v\n", result)
	sDom = result.HostedZone.Name
	fmt.Printf("Using zone: Hosted zone ID: %q, Domain name: %q, Description: %q\n", az.ZoneId, *sDom, *result.HostedZone.Config.Comment)

	if *iList {
		CnameList(svc, az.ZoneId, *sDom)
		return
	}

	aSvcRm := []string{}
	for _, s := range az.Services {
		sFqdn := s + "-" + az.Org + "." + *sDom
		Log("sFqdn %v\n", sFqdn)
		var err error
		if *iCr {
			err = ChangeResourceRecordSets(svc, az.ZoneId, sFqdn, "UPSERT")
		} else {
			err = ChangeResourceRecordSets(svc, az.ZoneId, sFqdn, "DELETE")
			if err != nil {
				log.Println(err)
				continue
			}
			aSvcRm = append(aSvcRm, s)
		}
		// if err != nil && *iForce {
		// 	log.Fatalf("Exiting due to error\n")
		// }
	}
	if *iDel && len(aSvcRm) > 0 && *iRun {
		az := AwsZone{ZoneId: *sZoneId, Org: *sOrg, ZoneDescr: *result.HostedZone.Config.Comment, Services: aSvcRm}
		Log("az %v\n", az)
		b, err := json.Marshal(az)
		if err != nil {
			log.Fatal(err)
		}
		Log("b %v\n", string(b))
		err = ioutil.WriteFile(*sFile, b, 0644)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func CnameList(svc *route53.Route53, sZoneId, sDom string) {
	listParams := &route53.ListResourceRecordSetsInput{
		HostedZoneId:    aws.String(sZoneId), // Required
		StartRecordName: aws.String(sDom),
		StartRecordType: aws.String(sCname),
	}
	respList, err := svc.ListResourceRecordSets(listParams)

	if err != nil {
		// fmt.Println(err.Error())
		log.Fatal(err)
	}

	Log("All records %v\n", respList)
	for _, v := range respList.ResourceRecordSets {
		if *v.Type != sCname {
			fmt.Printf("Skipping %v %v\n", *v.Type, *v.Name)
			continue
		}
		fmt.Printf("Record name %q routeTo %q ttl %v type %v\n", *v.Name, *v.ResourceRecords[0].Value, *v.TTL, *v.Type)
	}
}

func ChangeResourceRecordSets(svc *route53.Route53, sZoneId, sFqdn, sAction string) error {
	if *iRun {
		params := ChangeResourceRecordSetsInput(svc, sZoneId, sFqdn, sAction, *sDest)
		resp, err := svc.ChangeResourceRecordSets(&params)
		if err != nil {
			fmt.Printf("ChangeResourceRecordSets error: %v\n", err)
			return err
		}
		Log("ChangeResourceRecordSets %v\n", resp)
	}
	act := "was"
	if !*iRun {
		act = "will be"
	}
	fmt.Printf("Record %q %v %ved\n", sFqdn, act, sAction)
	return nil
}

func ChangeResourceRecordSetsInput(svc *route53.Route53, sZoneId, sFqdn, sAction, sDest string) route53.ChangeResourceRecordSetsInput {
	return route53.ChangeResourceRecordSetsInput{
		ChangeBatch: &route53.ChangeBatch{
			Changes: []*route53.Change{
				{
					Action: aws.String(sAction),
					ResourceRecordSet: &route53.ResourceRecordSet{
						Name: aws.String(sFqdn),
						Type: aws.String(sCname),
						ResourceRecords: []*route53.ResourceRecord{
							{
								Value: aws.String(sDest),
							},
						},
						TTL: aws.Int64(*TTL),
					},
				},
			},
		},
		HostedZoneId: aws.String(sZoneId),
	}
}

func Log(format string, a ...interface{}) {
	if *iDebug == true {
		fmt.Printf(format, a...)
	}
}

func b2i(b bool) int {
	if b {
		return 1
	}
	return 0
}
func s2i(s string) int {
	if s != "" {
		return 1
	}
	return 0
}
